class Player {
    constructor(location, height, width) {
        this.location = location;
        this.height = height;
        this.width = width;
        this.img = loadImage('assets/player.png');
    }

    display() {
        push();
        imageMode(CENTER);
        image(this.img, this.location.x -5, this.location.y, 100, 100);
        pop();
    }

    update() {
      let halfWidth = this.width / 2;
      this.location.x = constrain(mouseX, halfWidth, width);
    }
    
}