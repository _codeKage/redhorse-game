class GameManager {
 
    constructor() {
        this.player = new Player(createVector(mouseX, height - 50), 5, 20);
        this.enemies = [];
        this.score = 0;
    }

    render() {
        this.displayPlayer();
        this.generateEnemy();
        this.displayEnemies();
    }

    displayPlayer(){
        this.player.update();
        this.player.display();
        this.displayScore();
    }

    generateEnemy() {
        if(frameCount % 120 === 0){
            let enemyCount = Math.round(random(1,8)); 
            const xColumns = [60,120,180,240,300,360,420,500];
            
            for(let i = 0; i < enemyCount; i++){
            let columnIndex = Math.round(random(0,xColumns.length-1));
            let column = xColumns[columnIndex];
          
            xColumns.splice(columnIndex,1); 
          
            this.enemies.push(new Enemy(
                   createVector(column,-10),
                   100,
                   120
               ));
            }       
  
          }
  
    }

    displayScore() {
        textSize(30);
        fill(color(0, 0, 0));
        text("Score: "+Math.round(this.score),width/2.7,50);
    }

    displayEnemies() {
        this.enemies.forEach((enemy, index) => {
            if (enemy.onEdges()) {
                this.enemies.splice(index, 1);
                this.score++;
            } else if (enemy.collided(this.player.height - 30, this.player.location)) {
                alert('Dead');
                this.score = 0;
                this.enemies.splice(index, 1);
                location.reload();
            } else {
                try {
                    this.enemies[index].update();
                    this.enemies[index].display();
                } catch(exception) {

                }
                
            }
        });
    }
   
}