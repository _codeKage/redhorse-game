class Enemy {

    constructor(location, height, width) {
        this.location = location;
        this.height = height;
        this.width = width;
        this.velocity = createVector(0, 0.2);
        this.img = loadImage('assets/rh.png');
    }

    update() {
        this.location.add(this.velocity);
        this.location.y += 8;
    }

    display() {
        push();
        imageMode(CENTER);
        image(this.img, this.location.x, this.location.y, this.width, this.height);
        pop(); 
    }

    onEdges(){
        return this.location.y > (height + this.height);
    }

    collided(side,loc){
        return (side + this.height) > this.location.dist(loc);
    }

}